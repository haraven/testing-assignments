package repository;

import java.util.List;
import java.util.function.Predicate;

public interface Repository<T> {
    void add(T entity);
    List<T> filter(Predicate<T> pred);
    List<T> all();
    Boolean contains(T entity);
}
