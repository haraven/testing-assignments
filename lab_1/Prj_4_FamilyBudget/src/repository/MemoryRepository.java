package repository;

import utils.exceptions.validators.ValidationException;
import model.validators.Validator;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class MemoryRepository<T> implements Repository<T> {
    private List<T> entities;
    private Validator<T> validator;

    public MemoryRepository() {
        entities = new ArrayList<>();
    }

    public MemoryRepository(Validator<T> validator) {
        this();
        this.validator = validator;
    }

    public void setValidator(Validator<T> validator) {
        this.validator = validator;
    }

    @Override
    public void add(T entity) throws ValidationException {
        if (validator != null) {
            validator.validate(entity);
        }
        entities.add(entity);
    }

    @Override
    public List<T> filter(Predicate<T> pred) {
        return entities.stream()
                .filter(pred)
                .collect(Collectors.toList());
    }

    @Override
    public List<T> all() {
        return entities;
    }

    @Override
    public Boolean contains(T entity) {
        return entities.contains(entity);
    }
}
