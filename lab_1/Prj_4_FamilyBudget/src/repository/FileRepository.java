package repository;

import utils.exceptions.repository.RepositoryFileException;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public abstract class FileRepository<T> extends MemoryRepository<T> {
    private String filename;

    abstract T parseLine(String line);

    public void initFromFile(final String filename) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(filename))) {
            for (String line = bufferedReader.readLine(); line != null; line = bufferedReader.readLine()) {
                T entity = parseLine(line);
                add(entity);
            }
        } catch (Exception e) {
            throw new RepositoryFileException("Error while reading from file: " + filename, e);
        }
        this.filename = filename;
    }

    abstract String entityToFileFormat(T entity);

    private List<String> toFileFormat() {
        List<String> lines = new ArrayList<>();
        List<T> entities = all();
        entities.forEach(entity -> lines.add(entityToFileFormat(entity)));

        return lines;
    }

    public void saveToFile(final String file) {
        if (file == null || file.equals("")) throw new RepositoryFileException("Error saving to file: filename was null");
        saveToFileInternal(file, toFileFormat());
    }

    private void saveToFileInternal(final String filename, List<String> lines) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(filename, false))) {
            for (Integer i = 0; i < lines.size(); ++i) {
                bufferedWriter.write(lines.get(i));
                if (i < lines.size() - 1) {
                    bufferedWriter.newLine();
                }
            }
        } catch (Exception e)
        {
            throw new RepositoryFileException("Error while writing to file: " + filename, e);
        }
    }

    public String getFilename() {
        return filename;
    }
}
