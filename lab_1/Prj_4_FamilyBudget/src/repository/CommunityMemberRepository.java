package repository;

import utils.exceptions.repository.RepositoryAddException;
import utils.exceptions.repository.RepositoryFileException;
import utils.exceptions.validators.ValidationException;
import model.CommunityMember;

public class CommunityMemberRepository extends FileRepository<CommunityMember> {
    @Override
    CommunityMember parseLine(String line) throws RepositoryFileException {
        String[] tokens = line.split(";");
        if (tokens.length < CommunityMember.FIELD_COUNT || tokens.length > CommunityMember.FIELD_COUNT) {
            throw new RepositoryFileException("Error parsing line: " + line +
                    ". Invalid field count;" +
                    " expected: " + CommunityMember.FIELD_COUNT.toString() +
                    ", found: " + tokens.length);
        }

        Integer id = Integer.parseInt(tokens[0]);

        return new CommunityMember(id, tokens[1], tokens[2]);
    }

    @Override
    String entityToFileFormat(CommunityMember entity) {
        return entity.getId().toString() + ';' +
                entity.getFirstName() + ';' +
                entity.getLastName();
    }

    @Override
    public void add(CommunityMember entity) throws ValidationException {
        if (filter(communityMember -> communityMember.getId().equals(entity.getId())).size() > 0)
            throw new RepositoryAddException("Could not add member: " + entity + " because a member with the same ID already exists");
        super.add(entity);
    }
}
