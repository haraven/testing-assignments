package repository;

import utils.exceptions.repository.RepositoryFileException;
import model.BudgetEntry;
import utils.enums.BudgetEntryType;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BudgetEntryRepository extends FileRepository<BudgetEntry> {
    @Override
    BudgetEntry parseLine(String line) throws RepositoryFileException {
        String[] tokens = line.split(";");

        if (tokens.length < BudgetEntry.FIELD_COUNT || tokens.length > BudgetEntry.FIELD_COUNT) {
            throw new RepositoryFileException("Error parsing line: " + line +
                    ". Invalid field count;" +
                    " expected: " + BudgetEntry.FIELD_COUNT.toString() +
                    ", found: " + tokens.length);
        }

        BudgetEntryType entryType = BudgetEntryType.fromString(tokens[0]);
        Integer value = Integer.parseInt(tokens[1]);
        Integer memberId = Integer.parseInt(tokens[2]);
        DateFormat df = new SimpleDateFormat(BudgetEntry.DATE_FORMAT);
        try {
            Date date = df.parse(tokens[3]);
            return new BudgetEntry(entryType, value, memberId, date);
        } catch (ParseException e) {
            throw new RepositoryFileException("Error parsing line: " + line, e);
        }
    }

    @Override
    String entityToFileFormat(BudgetEntry entity) {
        DateFormat df = new SimpleDateFormat(BudgetEntry.DATE_FORMAT);
        return BudgetEntryType.toString(entity.getType()) + ';' +
                entity.getValue().toString() + ';' +
                entity.getMemberId().toString() + ';' +
                df.format(entity.getDateRecorded());
    }
}
