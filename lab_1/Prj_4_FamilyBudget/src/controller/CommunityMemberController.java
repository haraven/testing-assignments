package controller;

import model.CommunityMember;
import repository.FileRepository;
import repository.Repository;

import java.util.List;
import java.util.function.Predicate;

public class CommunityMemberController {
    private FileRepository<CommunityMember> memberRepository;

    public CommunityMemberController(FileRepository<CommunityMember> memberRepository) {
        this.memberRepository = memberRepository;
    }

    public void addMember(CommunityMember member) {
        memberRepository.add(member);
    }

    public List<CommunityMember> getAllMembers() {
        return memberRepository.all();
    }

    public List<CommunityMember> filterMembers(Predicate<CommunityMember> pred) {
        return memberRepository.filter(pred);
    }

    public void save(String filename) {
        memberRepository.saveToFile(filename);
    }

    public String getSaveFile() {
        return memberRepository.getFilename();
    }
}