package controller;

import model.BudgetEntry;
import repository.FileRepository;
import repository.Repository;

import java.util.List;
import java.util.function.Predicate;

public class BudgetEntryController {
    private FileRepository<BudgetEntry> entryRepository;

    public BudgetEntryController(FileRepository<BudgetEntry> entryRepository) {
        this.entryRepository = entryRepository;
    }

    public void addEntry(BudgetEntry entry) {
        entryRepository.add(entry);
    }

    public List<BudgetEntry> getAllEntries() {
        return entryRepository.all();
    }

    public List<BudgetEntry> filterEntries(Predicate<BudgetEntry> pred) {
        return entryRepository.filter(pred);
    }

    public void save(final String filename) {
        entryRepository.saveToFile(filename);
    }

    public String getSaveFile() {
        return entryRepository.getFilename();
    }
}
