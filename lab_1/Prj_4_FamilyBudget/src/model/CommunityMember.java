package model;

public class CommunityMember {
    public static final Integer FIELD_COUNT = 3;
    public static final Integer NAME_LEN_MAX = 50;

    private Integer id;
    private String firstName;
    private String lastName;

    public CommunityMember(Integer id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CommunityMember that = (CommunityMember) o;

        return id.equals(that.id) &&
                (firstName != null ? firstName.equals(that.firstName) : that.firstName == null) &&
                (lastName != null ? lastName.equals(that.lastName) : that.lastName == null);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "{" +
                "ID = " + id +
                ", first name = '" + firstName + '\'' +
                ", last name = '" + lastName + '\'' +
                '}';
    }
}
