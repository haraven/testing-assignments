package model.validators;

import utils.exceptions.validators.ValidationException;
import model.BudgetEntry;
import model.CommunityMember;
import repository.Repository;

import java.util.ArrayList;
import java.util.List;

public class BudgetEntryValidator extends Validator<BudgetEntry> {
    private Repository<CommunityMember> memberRepository;

    public BudgetEntryValidator(Repository<CommunityMember> memberRepository) {
        this.memberRepository = memberRepository;
    }

    @Override
    public void validate(BudgetEntry entity) throws ValidationException {
        List<String> errors = new ArrayList<>();
        if (entity.getMemberId() < 0 ||
                memberRepository.filter(
                        communityMember -> communityMember.getId().equals(entity.getMemberId()))
                        .size() == 0) {
            errors.add("No community member having ID: " + entity.getMemberId().toString() + " exists");
        }
        if (entity.getValue() < 0) {
            errors.add("Budget entry value cannot be negative");
        }

        if (errors.size()> 0) {
            throw new ValidationException(errorsToString(errors));
        }
    }
}
