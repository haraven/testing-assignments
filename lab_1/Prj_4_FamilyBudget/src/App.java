import controller.BudgetEntryController;
import controller.CommunityMemberController;
import model.CommunityMember;
import model.validators.BudgetEntryValidator;
import model.validators.CommunityMemberValidator;
import model.validators.Validator;
import repository.BudgetEntryRepository;
import repository.CommunityMemberRepository;
import ui.ConsoleMenu;

public class App {
    public static void main(String[] args) {

        Validator<CommunityMember> communityMemberValidator = new CommunityMemberValidator();
        CommunityMemberRepository communityMemberRepository = new CommunityMemberRepository();
        communityMemberRepository.setValidator(communityMemberValidator);
        communityMemberRepository.initFromFile("membersF.txt");

        BudgetEntryValidator budgetEntryValidator = new BudgetEntryValidator(communityMemberRepository);
        BudgetEntryRepository budgetEntryRepository = new BudgetEntryRepository();
        budgetEntryRepository.setValidator(budgetEntryValidator);
        budgetEntryRepository.initFromFile("budgetF.txt");

        CommunityMemberController communityMemberController = new CommunityMemberController(communityMemberRepository);
        BudgetEntryController budgetEntryController = new BudgetEntryController(budgetEntryRepository);

        ConsoleMenu console = new ConsoleMenu(communityMemberController, budgetEntryController);
        console.run();
    }
}
