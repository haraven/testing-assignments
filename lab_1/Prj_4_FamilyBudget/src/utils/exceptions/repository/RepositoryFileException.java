package utils.exceptions.repository;

public class RepositoryFileException extends RuntimeException {
    public RepositoryFileException() {
    }

    public RepositoryFileException(String message) {
        super(message);
    }

    public RepositoryFileException(String message, Throwable cause) {
        super(message, cause);
    }

    public RepositoryFileException(Throwable cause) {
        super(cause);
    }
}
