package utils.exceptions.utils;

public class BudgetEntryParseException extends RuntimeException {
    public BudgetEntryParseException() {
    }

    public BudgetEntryParseException(String message) {
        super(message);
    }

    public BudgetEntryParseException(String message, Throwable cause) {
        super(message, cause);
    }

    public BudgetEntryParseException(Throwable cause) {
        super(cause);
    }
}
