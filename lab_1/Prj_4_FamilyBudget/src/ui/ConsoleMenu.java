package ui;

import controller.BudgetEntryController;
import controller.CommunityMemberController;
import model.BudgetEntry;
import model.CommunityMember;
import utils.enums.BudgetEntryType;

import java.util.*;
import java.util.function.Consumer;

public class ConsoleMenu {
    private final static String ANSI_RED = "\u001B[31m";
    private final static String ANSI_WHITE = "\u001B[37m";

    private final static String DEFAULT_MEMBERS_FILE = "membersF.txt";
    private final static String DEFAULT_BUDGET_FILE = "budgetF.txt";

    private CommunityMemberController memberController;
    private BudgetEntryController budgetEntryController;
    private Scanner in;

    private Map<Integer, Consumer<Void>> commands;
    private List<String> commandDescriptions;

    private ConsoleMenu() {
        this.in = new Scanner(System.in);
        initCommands();
    }

    public ConsoleMenu(CommunityMemberController memberController, BudgetEntryController budgetEntryController) {
        this();
        this.memberController = memberController;
        this.budgetEntryController = budgetEntryController;
    }

    private void initCommands() {
        commands = new HashMap<>();

        commands.put(0, (Void) -> quit());
        commands.put(1, (Void) -> addMember());
        commands.put(2, (Void) -> addBudgetEntry());
        commands.put(3, (Void) -> listBudgetEntriesForMember());
        commands.put(4, (Void) -> listMembers());
        commands.put(5, (Void) -> listBudgetEntries());

        initCommandDescriptions();
    }

    private void initCommandDescriptions() {
        commandDescriptions = new ArrayList<>();
        commandDescriptions.add("exit");
        commandDescriptions.add("add a new member");
        commandDescriptions.add("add a new income/cost for a member");
        commandDescriptions.add("list the current income/cost for a member");
        commandDescriptions.add("list all members");
        commandDescriptions.add("list budget entries of the specified type (cost, income, all)");
    }

    private void printError(Exception e) {
        String errorMsg = e.getMessage();
        System.out.println();
        System.err.println(ANSI_RED + (errorMsg == null ? "Unknown error" : errorMsg) + ANSI_RED);
        System.out.println();
    }

    private void printMenu() {
        System.out.println(ANSI_WHITE + "Budget menu:" + ANSI_WHITE);
        for (Integer i = 0; i < commandDescriptions.size(); ++i) {
            String menuItem = "\t " + i.toString() + " - " + commandDescriptions.get(i);
            System.out.println(ANSI_WHITE + menuItem + ANSI_WHITE);
        }
    }

    private void addBudgetEntry() {
        System.out.print(ANSI_WHITE + "Enter budget entry type (\"cost\" or \"income\")\n> " + ANSI_WHITE);
        String entryType = in.nextLine();
        System.out.print("Enter value\n> ");
        Integer value = in.nextInt();
        in.nextLine();
        System.out.print("Enter member id\n> ");
        Integer memberId = in.nextInt();
        in.nextLine();

        BudgetEntry entry = new BudgetEntry(BudgetEntryType.fromString(entryType), value, memberId);
        budgetEntryController.addEntry(entry);
    }

    private Optional<BudgetEntry> readBudgetEntryFor(Integer memberId) {
        System.out.print(ANSI_WHITE + "Enter budget entry type (\"cost\" or \"income\"), or type \"end\" to stop\n> " + ANSI_WHITE);
        String entryType = in.nextLine();
        if (entryType.equals("end")) return Optional.empty();
        System.out.print("Enter value\n> ");
        Integer value = in.nextInt();
        in.nextLine();

        return Optional.of(new BudgetEntry(BudgetEntryType.fromString(entryType), value, memberId));
    }

    private void listBudgetEntriesForMember() {
        System.out.print("Enter member id\n> ");
        Integer memberId = in.nextInt();
        in.nextLine();
        List<BudgetEntry> entries = budgetEntryController.filterEntries(budgetEntry -> budgetEntry.getMemberId().equals(memberId));
        if (entries.size() > 0) {
            System.out.println("Entries for member " + memberId + ":");
            entries.forEach(System.out::println);
        } else {
            System.out.println("There are no entries for that member");
        }
    }

    private void listMembers() {
        List<CommunityMember> members = memberController.getAllMembers();
        if (members.size() > 0) {
            System.out.println("Members currently in the community:");
            members.forEach(System.out::println);
        } else {
            System.out.println("The community has no members");
        }
    }

    private void listBudgetEntries() {
        System.out.print("Enter the type of budget entry to display\n> ");
        String entryType = in.nextLine();
        List<BudgetEntry> entries;
        switch (entryType) {
            case "income":
                entries = budgetEntryController.filterEntries(budgetEntry -> budgetEntry.getType().equals(BudgetEntryType.ENTRY_INCOME));
                break;
            case "cost":
                entries = budgetEntryController.filterEntries(budgetEntry -> budgetEntry.getType().equals(BudgetEntryType.ENTRY_EXPENDITURE));
                break;
            case "all":
                entries = budgetEntryController.getAllEntries();
                break;
            default:
                System.out.println("Unknown command");
                return;
        }

        entries.forEach(System.out::println);
    }

    private void quit() {
        budgetEntryController.save(budgetEntryController.getSaveFile() != null ? budgetEntryController.getSaveFile() : DEFAULT_MEMBERS_FILE);
        memberController.save(memberController.getSaveFile() != null ? memberController.getSaveFile() : DEFAULT_BUDGET_FILE);
        System.exit(0);
    }

    private void addMember() {
        System.out.print(ANSI_WHITE + "Enter id\n> " + ANSI_WHITE);
        Integer id = in.nextInt();
        in.nextLine();
        System.out.print("Enter first name\n> ");
        String firstName = in.nextLine();
        System.out.print("Enter last name\n> ");
        String lastName = in.nextLine();

        CommunityMember member = new CommunityMember(id, firstName, lastName);
        memberController.addMember(member);

        System.out.println("Enter the person's income/expenditure (type \"end\" to stop)");
        for (Optional<BudgetEntry> entry = readBudgetEntryFor(id); entry.isPresent(); entry = readBudgetEntryFor(id)) {
            budgetEntryController.addEntry(entry.get());
        }
    }

    @SuppressWarnings("InfiniteLoopStatement")
    public void run() {
        while (true) {
            try {
                printMenu();
                System.out.print(ANSI_WHITE + "> " + ANSI_WHITE);
                int cmd = in.nextInt();
                in.nextLine();

                if (commands.containsKey(cmd)) {
                    commands.get(cmd).accept(null);
                } else {
                    System.err.println("Unknown command");
                }
            } catch (InputMismatchException e) {
                printError(e);
                in.nextLine();
            } catch (Exception e) {
                printError(e);
            }
        }
    }
}

