
@Narrative(
        title = "Filter products",
        text = { "In order to upgrade my PC",
                "As a tech enthusiast",
                "I want to be able to find only SSDs made by Kingston" },
        cardNumber = "#124"
)
package features.filter;

import net.thucydides.core.annotations.Narrative;
