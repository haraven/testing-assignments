package WikiExampleGroup.features.filter;

import WikiExampleGroup.steps.serenity.EndUser2;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.pages.Pages;
import net.thucydides.junit.annotations.Qualifier;
import net.thucydides.junit.annotations.UseTestDataFrom;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom("src/test/resources/FilterTestData.csv")
public class FilterByManufacturerStoryDdt {
    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @ManagedPages(defaultUrl = "http://www.pcgarage.ro/ssd/")
    public Pages pages;

    public String manufacturer;

    @Steps
    public EndUser2 endUser;

    @Issue("#FILTER-1")
    @Test
    public void filterSSDsByManufacturerDDT() {
        endUser.isOnTheHomePage();
        endUser.filtersByManufacturer(getManufacturer());
        endUser.shouldOnlySeeProductsBy(getManufacturer());
    }

    @Qualifier
    public String getQualifier() {
        return manufacturer;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
}
