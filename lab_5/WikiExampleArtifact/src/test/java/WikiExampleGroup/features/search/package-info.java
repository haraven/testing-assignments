
@Narrative(
        title = "Search for products",
        text = { "In order to upgrade my PC",
                "As a tech enthusiast",
                "I want to be able to find the best available products" },
        cardNumber = "#123"
)
package features.search;

import net.thucydides.core.annotations.Narrative;
