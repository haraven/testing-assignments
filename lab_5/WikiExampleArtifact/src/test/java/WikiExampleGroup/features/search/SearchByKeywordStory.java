package WikiExampleGroup.features.search;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import WikiExampleGroup.steps.serenity.EndUser;

@RunWith(SerenityRunner.class)
public class SearchByKeywordStory {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    public EndUser anna;

    @Issue("#WIKI-1")
    @Test
    public void searchingByKeywordShouldDisplayTheCorrespondingProduct() {
        anna.isOnTheHomePage();
        anna.looksFor("gtx 1080");
        anna.shouldSeeProduct("Placa video GIGABYTE GeForce GTX 1080 G1 GAMING 8GB DDR5X 256-bit");
    }

    @Test
    public void searchingByKeywordPearShouldDisplayTheCorrespondingArticle() {
        anna.isOnTheHomePage();
        anna.looksFor("doom");
        anna.shouldSeeProduct("Joc Bethesda Doom 3 BFG Edition pentru PC");
    }
} 