package WikiExampleGroup.features.search;

import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.pages.Pages;
import net.thucydides.junit.annotations.Qualifier;
import net.thucydides.junit.annotations.UseTestDataFrom;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import WikiExampleGroup.steps.serenity.EndUser;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom("src/test/resources/ProductsTestData.csv")
public class SearchByKeywordStoryDdt {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @ManagedPages(defaultUrl = "http://www.pcgarage.ro/")
    public Pages pages;

    public String name;
    public String product;
    
    @Qualifier
    public String getQualifier() {
        return name;
    }
    
    @Steps
    public EndUser endUser;

    @Issue("#WIKI-1")
    @Test
    public void searchWikiByKeywordTestDDT() {
        endUser.isOnTheHomePage();
		endUser.looksFor(getName());
        endUser.shouldSeeProduct(getProduct());
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	public String getProduct() {
		return product;
	}

	public void setDefinition(String definition) {
		this.product = definition;
	}

} 