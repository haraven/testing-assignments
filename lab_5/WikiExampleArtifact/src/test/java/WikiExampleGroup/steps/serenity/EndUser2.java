package WikiExampleGroup.steps.serenity;

import WikiExampleGroup.pages.PCGarageFilterResultPage;
import net.thucydides.core.annotations.Step;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;

public class EndUser2 {
    private PCGarageFilterResultPage pcGarageResultPage;

    @Step
    public void isOnTheHomePage() { pcGarageResultPage.open(); }

    @Step
    public void filtersByManufacturer(String name) {
        pcGarageResultPage.filterBy(name);
    }

    @Step
    public void shouldOnlySeeProductsBy(String manufacturer) {
        List<String> products = pcGarageResultPage.getProducts();
        products.forEach(product -> assertThat("Product should only be made by the given manufacturer", product.toLowerCase().contains(manufacturer.toLowerCase())));
    }
}
