package WikiExampleGroup.steps.serenity;

import WikiExampleGroup.pages.PCGarageSearchResultPage;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;

public class EndUser {

    private PCGarageSearchResultPage pcGarageSearchResultPage;

    @Step
    public void enters(String keyword) {
        pcGarageSearchResultPage.enterKeywords(keyword);
    }

    @Step
    public void startsSearch() {
        pcGarageSearchResultPage.lookupTerms();
    }

    @Step
    public void shouldSeeProduct(String title) {
        assertThat(pcGarageSearchResultPage.getProducts(), hasItem(containsString(title)));
    }

    @Step
    public void isOnTheHomePage() {
        pcGarageSearchResultPage.open();
    }

    @Step
    public void looksFor(String term) {
        enters(term);
        startsSearch();
    }
}