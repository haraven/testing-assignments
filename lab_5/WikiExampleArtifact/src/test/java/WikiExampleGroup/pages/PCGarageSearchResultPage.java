package WikiExampleGroup.pages;

import ch.lambdaj.function.convert.Converter;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import net.thucydides.core.pages.PageObject;

import java.util.List;

import static ch.lambdaj.Lambda.convert;

@DefaultUrl("http://www.pcgarage.ro/")
public class PCGarageSearchResultPage extends PageObject {
    @FindBy(name="search")
    private WebElementFacade searchTerms;

    @FindBy(id="sbbt")
    private WebElementFacade lookupButton;

    public void enterKeywords(String keyword) {
        searchTerms.type(keyword);
    }

    public void lookupTerms() {
        lookupButton.click();
    }

    public List<String> getProducts() {
        WebElementFacade grid = find(By.cssSelector("div.grid-products.clearfix.product-list-container"));
        List<WebElement> results = grid.findElements(By.className("product-box-container"));
        return convert(results, toStrings());
    }

    private Converter<WebElement, String> toStrings() {
        return new Converter<WebElement, String>() {
            public String convert(WebElement from) {
                return from.findElement(By.className("product-box")).findElement(By.className("pb-specs-container")).findElement(By.className("pb-name")).getText();
            }
        };
    }
}