package WikiExampleGroup.pages;

import ch.lambdaj.function.convert.Converter;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.Optional;

import static ch.lambdaj.Lambda.convert;

@DefaultUrl("http://www.pcgarage.ro")
public class PCGarageFilterResultPage extends PageObject {
    @FindBy(id = "ff_01")
    private WebElementFacade producersButton;

    public void filterBy(String manufacturer) {
        producersButton.click();
        WebElementFacade filterList = find(By.id("fv_01"));
        List<WebElement> filterOptions = filterList.findElements(By.tagName("li"));
        Optional<WebElement> manufacturerButton = filterOptions.stream()
            .filter(filterOption -> filterOption.findElement(By.tagName("a")).getText().toLowerCase().contains(manufacturer.toLowerCase()))
            .findFirst();
        assert(manufacturerButton.isPresent());

        manufacturerButton.get().click();
    }

    public List<String> getProducts() {
        WebElementFacade grid = find(By.cssSelector("div.grid-products.clearfix.product-list-container"));
        List<WebElement> results = grid.findElements(By.className("product-box-container"));
        return convert(results, toStrings());
    }

    private Converter<WebElement, String> toStrings() {
        return new Converter<WebElement, String>() {
            public String convert(WebElement from) {
                return from.findElement(By.className("product-box")).findElement(By.className("pb-specs-container")).findElement(By.className("pb-name")).getText();
            }
        };
    }
}
