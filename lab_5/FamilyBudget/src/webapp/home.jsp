<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Community members</title>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/themes/css/ui.css"/>
    <script src="${pageContext.request.contextPath}/themes/script/js/jquery-2.2.3.min.js"></script>
</head>
<body>
<script>
    $(document).ready(function () {
        $("#error").fadeOut(10000);
    });
</script>
<div id="content-wrapper">
    <form id="add-member-form" action="${pageContext.request.contextPath}/add_member" method="post" style="text-align: center;
        background-color: #090909;
        border: 1px solid #1e1e1e;
        ">
        <div id="error" style="z-index: 2; top: 2.5em; left: 43%;">
            ${error}
        </div>
        <div id="member-id-group">
            <span>
                ID:
            </span>
            <br/>
            <input type="number" name="member-id">
        </div>
        <div id="first-name-group">
            <span>
                First name:
            </span>
            <br/>
            <input type="text" name="first-name">
        </div>
        <div id="last-name-group">
            <span>
                Last name:
            </span>
            <br/>
            <input type="text" name="last-name">
        </div>
        <button type="submit" id="btn-add" style="margin-top: 1em;
                    margin-bottom: 1em;">
            ADD MEMBER
        </button>
    </form>
    <p class="welcome-msg">Currently added community members:</p>
    <div id="members" style="position: relative;
            top: 5%;
            max-height: 2000px;">
        <c:forEach items="${members}" var="item">
            <p>${item}</p>
        </c:forEach>
    </div>
    <br/>
    <br/>
</div>
</body>
</html>
