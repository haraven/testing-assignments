package features.search;

import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.pages.Pages;
import net.thucydides.junit.annotations.Qualifier;
import net.thucydides.junit.annotations.UseTestDataFrom;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import steps.serenity.EndUser;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom("src/test/resources/MemberTestData.csv")
public class AddCommunityMemberDdt {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @ManagedPages(defaultUrl = "http://localhost:1338/add_member")
    public Pages pages;

    public Integer id;
    public String firstName;
    public String lastName;
    
    @Qualifier
    public String getQualifier() {
        return id.toString();
    }
    
    @Steps
    public EndUser endUser;

    @Issue("#ADDMEMBER-1")
    @Test
    public void searchWikiByKeywordTestDDT() {
        endUser.isOnTheHomePage();
		endUser.adds(getId(), getFirstName(), getLastName());
        endUser.shouldSeeMember(getFirstName() + " " + getLastName());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
