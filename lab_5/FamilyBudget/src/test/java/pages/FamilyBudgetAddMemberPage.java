package pages;

import ch.lambdaj.function.convert.Converter;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import net.thucydides.core.pages.PageObject;

import java.util.List;

import static ch.lambdaj.Lambda.convert;

@DefaultUrl("http://www.pcgarage.ro/")
public class FamilyBudgetAddMemberPage extends PageObject {
    @FindBy(name="member-id")
    private WebElementFacade idInput;
    @FindBy(name="first-name")
    private WebElementFacade firstNameInput;
    @FindBy(name="last-name")
    private WebElementFacade lastNameInput;

    @FindBy(id="btn-add")
    private WebElementFacade addBtn;

    public void enterFields(Integer id, String firstName, String lastName) {
        idInput.type(id.toString());
        firstNameInput.type(firstName);
        lastNameInput.type(lastName);
    }

    public void addMember() {
        addBtn.click();
    }

    public List<String> getMembers() {
        WebElementFacade memberList = find(By.id("members"));
        List<WebElement> results = memberList.findElements(By.tagName("p"));
        return convert(results, toStrings());
    }

    private Converter<WebElement, String> toStrings() {
        return WebElement::getText;
    }
}