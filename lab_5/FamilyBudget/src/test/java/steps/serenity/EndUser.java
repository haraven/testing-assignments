package steps.serenity;

import net.thucydides.core.annotations.Step;
import pages.FamilyBudgetAddMemberPage;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;

public class EndUser {
    private FamilyBudgetAddMemberPage familyBudgetAddMemberPage;

    @Step
    public void enters(Integer id, String firstName, String lastName) {
        familyBudgetAddMemberPage.enterFields(id, firstName, lastName);
    }

    @Step
    public void pressesAdd() {
        familyBudgetAddMemberPage.addMember();
    }

    @Step
    public void shouldSeeMember(String name) {
        List<String> memberNames = familyBudgetAddMemberPage.getMembers();
        Optional<String> member = memberNames.stream()
                .filter(memberName -> memberName.toLowerCase().equals(name.toLowerCase()))
                .findFirst();

        assertThat("Member was not found in the member list", member.isPresent());
    }

    @Step
    public void isOnTheHomePage() {
        familyBudgetAddMemberPage.open();
    }

    @Step
    public void adds(Integer id, String firstName, String lastName) {
        enters(id, firstName, lastName);
        pressesAdd();
    }
}