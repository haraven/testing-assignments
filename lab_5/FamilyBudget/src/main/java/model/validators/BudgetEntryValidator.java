package model.validators;

import model.BudgetEntry;
import repository.CommunityMemberRepository;
import utils.exceptions.validators.ValidationException;

import java.util.ArrayList;
import java.util.List;

public class BudgetEntryValidator extends Validator<BudgetEntry> {
    private CommunityMemberRepository memberRepository;

    public BudgetEntryValidator(CommunityMemberRepository memberRepository) {
        this.memberRepository = memberRepository;
    }

    @Override
    public void validate(BudgetEntry entity) throws ValidationException {
        List<String> errors = new ArrayList<>();
        if (entity.getMemberId() < 0 ||
                memberRepository.findById(entity.getMemberId()) == null) {
            errors.add("No community member having ID: " + entity.getMemberId().toString() + " exists");
        }
        if (entity.getValue() < 0) {
            errors.add("Budget entry value cannot be negative");
        }

        if (errors.size() > 0) {
            throw new ValidationException(errorsToString(errors));
        }
    }
}
