package repository;

import model.BudgetEntry;
import model.validators.Validator;
import utils.enums.BudgetEntryType;
import utils.exceptions.repository.RepositoryFileException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BudgetEntryRepository extends FileRepository<BudgetEntry> {
    public BudgetEntryRepository() {
        super();
    }

    public BudgetEntryRepository(Validator<BudgetEntry> validator) {
        super(validator);
    }

    public List<BudgetEntry> budgetEntriesForMember(Integer memberId) {
        List<BudgetEntry> entriesForMember = new ArrayList<>();

        for (BudgetEntry entry : entities) {
            if (entry.getMemberId().equals(memberId)) {
                entriesForMember.add(entry);
            }
        }

        return entriesForMember;
    }

    @Override
    BudgetEntry parseLine(String line) throws RepositoryFileException {
        String[] tokens = line.split(";");

        if (tokens.length < BudgetEntry.FIELD_COUNT || tokens.length > BudgetEntry.FIELD_COUNT) {
            throw new RepositoryFileException("Error parsing line: " + line +
                    ". Invalid field count;" +
                    " expected: " + BudgetEntry.FIELD_COUNT.toString() +
                    ", found: " + tokens.length);
        }

        BudgetEntryType entryType = BudgetEntryType.fromString(tokens[0]);
        Integer value = Integer.parseInt(tokens[1]);
        Integer memberId = Integer.parseInt(tokens[2]);
        DateFormat df = new SimpleDateFormat(BudgetEntry.DATE_FORMAT);
        try {
            Date date = df.parse(tokens[3]);
            return new BudgetEntry(entryType, value, memberId, date);
        } catch (ParseException e) {
            throw new RepositoryFileException("Error parsing line: " + line, e);
        }
    }

    @Override
    String entityToFileFormat(BudgetEntry entity) {
        DateFormat df = new SimpleDateFormat(BudgetEntry.DATE_FORMAT);
        return BudgetEntryType.toString(entity.getType()) + ';' +
                entity.getValue().toString() + ';' +
                entity.getMemberId().toString() + ';' +
                df.format(entity.getDateRecorded());
    }
}
