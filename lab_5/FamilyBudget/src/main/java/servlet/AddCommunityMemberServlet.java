package servlet;

import controller.CommunityMemberController;
import model.CommunityMember;
import utils.exceptions.validators.ValidationException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet({"/add_member", "/add_member/"})
public class AddCommunityMemberServlet extends HttpServlet {
    private CommunityMemberController communityMemberController;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        communityMemberController = CommunityMemberController.instance();
    }

    private List<String> membersToString(List<CommunityMember> members) {
        return members.stream()
            .map(member -> member.getFirstName() + " " + member.getLastName())
            .sorted()
            .collect(Collectors.toList());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<String> members = membersToString(communityMemberController.getAllMembers());
        request.setAttribute("members", members);
        request.getRequestDispatcher("/home.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String firstName = request.getParameter("first-name");
            String lastName = request.getParameter("last-name");
            Integer id = Integer.parseInt(request.getParameter("member-id"));
            CommunityMember member = CommunityMember.builder()
                    .id(id)
                    .firstName(firstName)
                    .lastName(lastName)
                    .build();
            communityMemberController.addMember(member);
        } catch (ValidationException ex) {
            request.setAttribute("error", "Error while adding a new community member: " + ex.getMessage());
        } catch (RuntimeException ex) {
            request.setAttribute("error", "Internal error: " + ex.getMessage());
        }

        doGet(request, response);
    }

    @Override
    public void destroy() {
        // UNCOMMENT ME IF LEGIT SHOWCASING
        //communityMemberController.save(communityMemberController.getSaveFile());

        super.destroy();
    }
}
