package controller;

import model.CommunityMember;
import model.validators.CommunityMemberValidator;
import model.validators.Validator;
import repository.CommunityMemberRepository;
import utils.exceptions.validators.ValidationException;

import java.io.File;
import java.util.List;
//import java.util.function.Predicate;

public class CommunityMemberController {
    private static CommunityMemberController controller = null;

    private CommunityMemberRepository memberRepository;

    public CommunityMemberController(CommunityMemberRepository memberRepository) {
        this.memberRepository = memberRepository;
    }

    public void addMember(CommunityMember member) throws ValidationException {
        memberRepository.add(member);
    }

    public List<CommunityMember> getAllMembers() {
        return memberRepository.all();
    }

    public CommunityMember findById(Integer ID) {
        return memberRepository.findById(ID);
    }

//    public List<CommunityMember> filterMembers(Predicate<CommunityMember> pred) {
//        return memberRepository.filter(pred);
//    }

    public CommunityMemberRepository getMemberRepository() {
        return memberRepository;
    }

    public void save(String filename) {
        memberRepository.saveToFile(filename);
    }

    public String getSaveFile() {
        return memberRepository.getFilename();
    }

    public static CommunityMemberController instance() {
        if (controller == null) {
            Validator<CommunityMember> communityMemberValidator = new CommunityMemberValidator();
            CommunityMemberRepository communityMemberRepository = new CommunityMemberRepository();
            communityMemberRepository.setValidator(communityMemberValidator);
            ClassLoader classLoader = CommunityMember.class.getClassLoader();
            File file = new File(classLoader.getResource("membersF.txt").getFile());
            communityMemberRepository.initFromFile(file.getAbsolutePath());
            controller = new CommunityMemberController(communityMemberRepository);
        }

        return controller;
    }
}