package crie1698_MV.controller;

import crie1698_MV.model.BudgetEntry;
import crie1698_MV.model.CommunityMember;
import crie1698_MV.utils.enums.BudgetEntryType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class BigBangTest {
    private BudgetEntryController budgetEntryController;
    private CommunityMemberController communityMemberController;

    @Before
    public void setUp() throws Exception {
        communityMemberController = CommunityMemberControllerTest.initDummyController();
        budgetEntryController = BudgetEntryControllerTest.initDummyController(communityMemberController.getMemberRepository());
    }

    @After
    public void tearDown() throws Exception {
        budgetEntryController = null;
        communityMemberController = null;
    }

    @Test
    public void testAddCommunityMember() throws Exception {
        CommunityMember validCommunityMember = new CommunityMember(3, "Dean", "Winchester");

        communityMemberController.addMember(validCommunityMember);
        assertTrue("Community member " + validCommunityMember + " was supposed to have been added, but was not found after adding it", communityMemberController.findById(validCommunityMember.getId()) != null);
    }

    @Test
    public void testAddBudgetEntry() throws Exception {
        BudgetEntry validBudgetEntry = new BudgetEntry(BudgetEntryType.ENTRY_EXPENDITURE, 50, 1);
        budgetEntryController.addEntry(validBudgetEntry);
        assertTrue(budgetEntryController.getAllEntries().contains(validBudgetEntry));
    }

    @Test
    public void testGetBudgetEntryForMember() throws Exception {
        int memberId = 1;

        BudgetEntry validBudgetEntry = new BudgetEntry(BudgetEntryType.ENTRY_EXPENDITURE, 1337, memberId);
        budgetEntryController.addEntry(validBudgetEntry);
        List<BudgetEntry> budgetEntries = budgetEntryController.budgetEntriesForMember(memberId);

        assertTrue("Budget entry " + validBudgetEntry + " was not retrieved, or the list contained too many entries", budgetEntries != null && budgetEntries.size() == 1 && budgetEntries.contains(validBudgetEntry));
    }

    @Test
    @SuppressWarnings("Duplicates")
    public void testAllIntegration() throws  Exception {
        int memberId = 3;
        CommunityMember validCommunityMember = new CommunityMember(memberId, "Dean", "Winchester");

        communityMemberController.addMember(validCommunityMember);

        BudgetEntry validBudgetEntry = new BudgetEntry(BudgetEntryType.ENTRY_EXPENDITURE, 50, memberId);
        budgetEntryController.addEntry(validBudgetEntry);

        List<BudgetEntry> budgetEntries = budgetEntryController.budgetEntriesForMember(memberId);

        assertTrue("Integration test faled", budgetEntries != null && budgetEntries.size() == 1 && budgetEntries.contains(validBudgetEntry) && budgetEntries.get(0).getMemberId().equals(memberId));
    }
}
