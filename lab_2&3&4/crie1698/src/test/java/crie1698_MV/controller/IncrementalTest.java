package crie1698_MV.controller;

import crie1698_MV.model.BudgetEntry;
import crie1698_MV.model.CommunityMember;
import crie1698_MV.model.validators.BudgetEntryValidator;
import crie1698_MV.model.validators.CommunityMemberValidator;
import crie1698_MV.model.validators.Validator;
import crie1698_MV.repository.BudgetEntryRepository;
import crie1698_MV.repository.CommunityMemberRepository;
import crie1698_MV.utils.enums.BudgetEntryType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class IncrementalTest {
    private BudgetEntryController budgetEntryController;
    private CommunityMemberController communityMemberController;

    @Before
    public void setUp() throws Exception {
        communityMemberController = CommunityMemberControllerTest.initDummyController();
        budgetEntryController = BudgetEntryControllerTest.initDummyController(communityMemberController.getMemberRepository());
    }

    @After
    public void tearDown() throws Exception {
        budgetEntryController = null;
        communityMemberController = null;
    }

    @Test
    public void testAddCommunityMember() throws Exception {
        CommunityMember validCommunityMember = new CommunityMember(3, "Dean", "Winchester");

        communityMemberController.addMember(validCommunityMember);
        assertTrue("Community member " + validCommunityMember + " was supposed to have been added, but was not found after adding it", communityMemberController.findById(validCommunityMember.getId()) != null);
    }

    @Test
    public void testAddBudgetEntryIncremental() throws Exception {
        int memberId = 3;

        CommunityMember validCommunityMember3 = new CommunityMember(memberId, "Dean", "Winchester");
        communityMemberController.addMember(validCommunityMember3);

        BudgetEntry validBudgetEntry = new BudgetEntry(BudgetEntryType.ENTRY_EXPENDITURE, 50, memberId);
        budgetEntryController.addEntry(validBudgetEntry);
        assertTrue(budgetEntryController.getAllEntries().contains(validBudgetEntry));
    }

    @Test
    public void testGetBudgetEntryForMemberIncremental() throws Exception {
        int memberId = 3;

        CommunityMember validCommunityMember3 = new CommunityMember(memberId, "Dean", "Winchester");
        communityMemberController.addMember(validCommunityMember3);

        BudgetEntry validBudgetEntry = new BudgetEntry(BudgetEntryType.ENTRY_EXPENDITURE, 50, memberId);
        budgetEntryController.addEntry(validBudgetEntry);

        List<BudgetEntry> foundBudgetEntries = budgetEntryController.budgetEntriesForMember(memberId);
        assertTrue("Budget entry " + validBudgetEntry + " was not retrieved, or the list contained too many entries", foundBudgetEntries != null && foundBudgetEntries.size() == 1 && foundBudgetEntries.contains(validBudgetEntry));
    }
}
