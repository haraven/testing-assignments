package crie1698_MV.controller;

import crie1698_MV.model.BudgetEntry;
import crie1698_MV.model.CommunityMember;
import crie1698_MV.model.validators.BudgetEntryValidator;
import crie1698_MV.model.validators.CommunityMemberValidator;
import crie1698_MV.model.validators.Validator;
import crie1698_MV.repository.BudgetEntryRepository;
import crie1698_MV.repository.CommunityMemberRepository;
import crie1698_MV.repository.FileRepository;
import crie1698_MV.repository.Repository;
import crie1698_MV.utils.enums.BudgetEntryType;
import crie1698_MV.utils.exceptions.validators.ValidationException;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;
import java.util.Random;

import static org.junit.Assert.*;

public class BudgetEntryControllerTest {
    private BudgetEntryController budgetEntryController;

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    public static BudgetEntryController initDummyController(CommunityMemberRepository communityMemberRepository) {
        communityMemberRepository.add(new CommunityMember(0, "Bob", "Bobberson"));
        communityMemberRepository.add(new CommunityMember(1, "Sally", "Brown"));

        Validator<BudgetEntry> budgetEntryValidator = new BudgetEntryValidator(communityMemberRepository);
        BudgetEntryRepository budgetEntryRepository = new BudgetEntryRepository(budgetEntryValidator);
        return new BudgetEntryController(budgetEntryRepository);
    }

    @Before
    public void setUp() throws Exception {
        Validator<CommunityMember> communityMemberValidator = new CommunityMemberValidator();
        CommunityMemberRepository communityMemberRepository = new CommunityMemberRepository(communityMemberValidator);

        budgetEntryController = BudgetEntryControllerTest.initDummyController(communityMemberRepository);
    }

    @After
    public void tearDown() throws Exception {
        budgetEntryController = null;
    }

    @Test
    public void addValidEntry() throws Exception {
        BudgetEntry validBudgetEntry = new BudgetEntry(BudgetEntryType.ENTRY_EXPENDITURE, 50, 1);
        budgetEntryController.addEntry(validBudgetEntry);
        assertTrue(budgetEntryController.getAllEntries().contains(validBudgetEntry));
    }

    @Test
    public void addValidEntryNoValidator() throws Exception {
        BudgetEntryRepository budgetEntryRepository = new BudgetEntryRepository();
        budgetEntryController = new BudgetEntryController(budgetEntryRepository);
        Random random = new Random();

        BudgetEntry validBudgetEntry = new BudgetEntry(BudgetEntryType.ENTRY_EXPENDITURE, random.nextInt(), random.nextInt());
        budgetEntryController.addEntry(validBudgetEntry);
        assertTrue(budgetEntryController.getAllEntries().contains(validBudgetEntry));
    }

    @Test
    public void addinvalidEntryNegativeMemberId() throws Exception {
        BudgetEntry invalidBudgetEntry1 = new BudgetEntry(BudgetEntryType.ENTRY_INCOME, 50, -1);
        // expecting the following controller calls to fail
        expectedException.expect(ValidationException.class);
        budgetEntryController.addEntry(invalidBudgetEntry1);
    }

    @Test
    public void addInvalidEntryNonexistentMemberId() throws Exception {
        BudgetEntry invalidBudgetEntry2 = new BudgetEntry(BudgetEntryType.ENTRY_EXPENDITURE, 50, 1337);
        // expecting the following controller calls to fail
        expectedException.expect(ValidationException.class);
        budgetEntryController.addEntry(invalidBudgetEntry2);
    }

    @Test
    public void addInvalidEntryNegativeValue() throws Exception {
        BudgetEntry invalidBudgetEntry3 = new BudgetEntry(BudgetEntryType.ENTRY_EXPENDITURE, -30, 1);
        // expecting the following controller calls to fail
        expectedException.expect(ValidationException.class);
        budgetEntryController.addEntry(invalidBudgetEntry3);
    }

    @Test
    public void addInvalidEntryNegativeEverything() throws Exception {
        BudgetEntry invalidBudgetEntry4 = new BudgetEntry(BudgetEntryType.ENTRY_EXPENDITURE, -30, -1);
        // expecting the following controller calls to fail
        expectedException.expect(ValidationException.class);
        budgetEntryController.addEntry(invalidBudgetEntry4);
    }

    @Test
    public void getAllEntries() throws Exception {
        BudgetEntry validBudgetEntry = new BudgetEntry(BudgetEntryType.ENTRY_INCOME, 50, 1);
        budgetEntryController.addEntry(validBudgetEntry);
        List<BudgetEntry> budgetEntries = budgetEntryController.getAllEntries();
        assertTrue("Budget entry " + validBudgetEntry + " was not found in the list of budget entries", budgetEntries != null && budgetEntries.contains(validBudgetEntry));
    }

    @Test
    public void budgetEntriesForMember() throws Exception {
        Integer memberId = 1;

        BudgetEntry validBudgetEntry1 = new BudgetEntry(BudgetEntryType.ENTRY_INCOME, 50, memberId);
        BudgetEntry validBudgetEntry2 = new BudgetEntry(BudgetEntryType.ENTRY_EXPENDITURE, 51, memberId);

        budgetEntryController.addEntry(validBudgetEntry1);
        budgetEntryController.addEntry(validBudgetEntry2);

        List<BudgetEntry> entriesForMember = budgetEntryController.budgetEntriesForMember(memberId);
        assertTrue("Budget entries missing for member ID" + memberId, entriesForMember != null && entriesForMember.contains(validBudgetEntry1) && entriesForMember.contains(validBudgetEntry2));
    }
}
