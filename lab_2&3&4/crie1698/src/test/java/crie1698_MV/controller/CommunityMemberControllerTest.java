package crie1698_MV.controller;

import crie1698_MV.model.CommunityMember;
import crie1698_MV.model.validators.CommunityMemberValidator;
import crie1698_MV.model.validators.Validator;
import crie1698_MV.repository.CommunityMemberRepository;
import crie1698_MV.repository.FileRepository;
import crie1698_MV.utils.exceptions.repository.RepositoryAddException;
import crie1698_MV.utils.exceptions.validators.ValidationException;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class CommunityMemberControllerTest {
    private CommunityMemberController communityMemberController;

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    public static CommunityMemberController initDummyController() {
        Validator<CommunityMember> validator = new CommunityMemberValidator();
        CommunityMemberRepository communityMemberRepository = new CommunityMemberRepository(validator);
        return new CommunityMemberController(communityMemberRepository);
    }

    @Before
    public void setUp() throws Exception {
        communityMemberController = CommunityMemberControllerTest.initDummyController();
    }

    @After
    public void tearDown() throws Exception {
        communityMemberController = null;
    }

    @Test
    public void addValidMemberMinID() throws Exception {
        CommunityMember validCommunityMember1 = new CommunityMember(0, "Bob", "Bobberson");
        communityMemberController.addMember(validCommunityMember1);
        assertTrue("Community member " + validCommunityMember1 + " was supposed to have been added, but was not found after adding it", communityMemberController.findById(validCommunityMember1.getId()) != null);
    }

    @Test
    public void addValidMemberMaxID() throws Exception {
        CommunityMember validCommunityMember2 = new CommunityMember(Integer.MAX_VALUE, "Sally", "Brown");
        // Req_EC_BVA_all_TC: 10
        communityMemberController.addMember(validCommunityMember2);
        assertTrue("Community member " + validCommunityMember2 + " was supposed to have been added, but was not found after adding it", communityMemberController.findById(validCommunityMember2.getId()) != null);
    }

    @Test
    public void addValidMemberNaturalNumberID() throws Exception {
        CommunityMember validCommunityMember3 = new CommunityMember(1, "Sally", "Brown");
        // Req_EC_BVA_all_TC: 11
        communityMemberController.addMember(validCommunityMember3);
        assertTrue("Community member " + validCommunityMember3 + " was supposed to have been added, but was not found after adding it", communityMemberController.findById(validCommunityMember3.getId()) != null);
    }

    @Test
    public void addValidMemberMaxMinusOneID() throws Exception {
        CommunityMember validCommunityMember4 = new CommunityMember(Integer.MAX_VALUE - 1, "Dean", "Winchester");
        // Req_EC_BVA_all_TC: 12
        communityMemberController.addMember(validCommunityMember4);
        assertTrue("Community member " + validCommunityMember4 + " was supposed to have been added, but was not found after adding it", communityMemberController.findById(validCommunityMember4.getId()) != null);
    }

    @Test
    public void addInvalidMemberNegativeID() throws Exception {
        CommunityMember invalidCommunityMember1 = new CommunityMember(-1, "Sally", "Brown");
		 // expecting the following controller calls to fail
        expectedException.expect(ValidationException.class);
        // Req_EC_BVA_all_TC: 2
        communityMemberController.addMember(invalidCommunityMember1);
    }

    @Test
    public void addInvalidMemberInvalidCharactersInFirstName() throws Exception {
        CommunityMember invalidCommunityMember2 = new CommunityMember(6, "B0b", "Bobberson");
		 // expecting the following controller calls to fail
        expectedException.expect(ValidationException.class);
        // Req_EC_BVA_all_TC: 3
        communityMemberController.addMember(invalidCommunityMember2);
    }

    @Test
    public void addInvalidMemberInvalidCharactersInLastName() throws Exception {
        CommunityMember invalidCommunityMember3 = new CommunityMember(6, "Bob", "B0bberson");
        // expecting the following controller calls to fail
        expectedException.expect(ValidationException.class);
        // Req_EC_BVA_all_TC: 4
        communityMemberController.addMember(invalidCommunityMember3);
    }

    @Test
    public void addInvalidMemberAlreadyExists() throws Exception {
        CommunityMember validCommunityMember1 = new CommunityMember(0, "Bob", "Bobberson");
        communityMemberController.addMember(validCommunityMember1);
        CommunityMember invalidCommunityMember4 = new CommunityMember(0, "Sally", "Brown");
        // expecting the following controller calls to fail
        expectedException.expect(RepositoryAddException.class);
        // Req_EC_BVA_all_TC: 5
        communityMemberController.addMember(invalidCommunityMember4);
    }

    @Test
    public void addInvalidMemberNullFirstName() throws Exception {
        CommunityMember invalidCommunityMember5 = new CommunityMember(6, "", "Bobberson");
		 // expecting the following controller calls to fail
        expectedException.expect(ValidationException.class);
        // Req_EC_BVA_all_TC: 6
        communityMemberController.addMember(invalidCommunityMember5);
    }

    @Test
    public void addInvalidMemberNullLastName() throws Exception {
        CommunityMember invalidCommunityMember7 = new CommunityMember(6, "Bob", "");
        // expecting the following controller calls to fail
        expectedException.expect(ValidationException.class);
        // Req_EC_BVA_all_TC: 8
        communityMemberController.addMember(invalidCommunityMember7);
    }

    @Test
    public void addInvalidMemberLongFirstName() throws Exception {
        CommunityMember invalidCommunityMember6 = new CommunityMember(6, "Booooooooooooooooooooooooooooooooooooooooooooooooob", "Bobberson");
		 // expecting the following controller calls to fail
        expectedException.expect(ValidationException.class);
        // Req_EC_BVA_all_TC: 7
        communityMemberController.addMember(invalidCommunityMember6);
    }

    @Test
    public void addInvalidMemberLongLastName() throws Exception {
        CommunityMember invalidCommunityMember8 = new CommunityMember(6, "Bob", "Booooooooooooooooooooooooooooooooooooooooooobberson");
        // expecting the following controller calls to fail
        expectedException.expect(ValidationException.class);
        // Req_EC_BVA_all_TC: 9
        communityMemberController.addMember(invalidCommunityMember8);
    }

    @Test
    public void getAllMembers() throws Exception {
        List<CommunityMember> communityMembers = communityMemberController.getAllMembers();
        assertTrue(communityMembers != null && communityMembers.size() == 0);

        CommunityMember validCommunityMember = new CommunityMember(5, "Bob", "Bobberson");
        communityMemberController.addMember(validCommunityMember);
        communityMembers = communityMemberController.getAllMembers();
        assertTrue("Community member " + validCommunityMember + " was not found in the list of community members", communityMembers != null && communityMembers.contains(validCommunityMember));
    }

    @Test
    public void findById() throws Exception {
        CommunityMember validCommunityMember1 = new CommunityMember(5, "Dean", "Winchester");
        CommunityMember validCommunityMember2 = new CommunityMember(6, "Sam", "Winchester");
        CommunityMember validCommunityMember3 = new CommunityMember(7, "Bobby", "Singer");
        communityMemberController.addMember(validCommunityMember1);
        communityMemberController.addMember(validCommunityMember2);
        communityMemberController.addMember(validCommunityMember3);
        CommunityMember foundCommunityMember2 = communityMemberController.findById(validCommunityMember2.getId());
        assertTrue("Community member " + validCommunityMember2 + " was not found in the list of community members", foundCommunityMember2.equals(validCommunityMember2));
    }
}
