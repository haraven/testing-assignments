package crie1698_MV.repository;

import crie1698_MV.model.validators.Validator;
import crie1698_MV.utils.exceptions.validators.ValidationException;

import java.util.ArrayList;
import java.util.List;

public abstract class MemoryRepository<T> implements Repository<T> {
    protected List<T> entities;
    private Validator<T> validator;

    public MemoryRepository() {
        entities = new ArrayList<>();
    }

    public MemoryRepository(Validator<T> validator) {
        this();
        this.validator = validator;
    }

    public void setValidator(Validator<T> validator) {
        this.validator = validator;
    }

    @Override
    public void add(T entity) throws ValidationException {
        if (validator != null) {
            validator.validate(entity);
        }
        entities.add(entity);
    }

//    @Override
//    public List<T> filter(Predicate<T> pred) {
//        return entities.stream()
//                .filter(pred)
//                .collect(Collectors.toList());
//    }

    @Override
    public List<T> all() {
        return entities;
    }

    @Override
    public Boolean contains(T entity) {
        return entities.contains(entity);
    }
}
