package crie1698_MV.repository;

import crie1698_MV.model.CommunityMember;
import crie1698_MV.model.validators.Validator;
import crie1698_MV.utils.exceptions.repository.RepositoryAddException;
import crie1698_MV.utils.exceptions.repository.RepositoryFileException;
import crie1698_MV.utils.exceptions.validators.ValidationException;

public class CommunityMemberRepository extends FileRepository<CommunityMember> {
    public CommunityMemberRepository() {
        super();
    }

    public CommunityMemberRepository(Validator<CommunityMember> validator) {
        super(validator);
    }

    @Override
    CommunityMember parseLine(String line) throws RepositoryFileException {
        String[] tokens = line.split(";");
        if (tokens.length < CommunityMember.FIELD_COUNT || tokens.length > CommunityMember.FIELD_COUNT) {
            throw new RepositoryFileException("Error parsing line: " + line +
                    ". Invalid field count;" +
                    " expected: " + CommunityMember.FIELD_COUNT.toString() +
                    ", found: " + tokens.length);
        }

        Integer id = Integer.parseInt(tokens[0]);

        return new CommunityMember(id, tokens[1], tokens[2]);
    }

    @Override
    String entityToFileFormat(CommunityMember entity) {
        return entity.getId().toString() + ';' +
                entity.getFirstName() + ';' +
                entity.getLastName();
    }

    @Override
    public void add(CommunityMember entity) throws ValidationException {
        if (findById(entity.getId()) != null)
            throw new RepositoryAddException("Could not add member: " + entity + " because a member with the same ID already exists");
        super.add(entity);
    }

    public CommunityMember findById(Integer ID) {
        for (CommunityMember entity : entities)
            if (entity.getId().equals(ID))
                return entity;

        return null;
    }
}
