package crie1698_MV.utils.enums;


import crie1698_MV.utils.exceptions.utils.BudgetEntryParseException;

public enum BudgetEntryType {
    ENTRY_INCOME,
    ENTRY_EXPENDITURE;

    public static BudgetEntryType fromString(String entryTypeString) {
        switch (entryTypeString) {
            case "income":
                return ENTRY_INCOME;
            case "cost":
                return ENTRY_EXPENDITURE;
            default: throw new BudgetEntryParseException("Could not convert string: " + entryTypeString + " to BudgetEntryType");
        }
    }

    public static String toString(BudgetEntryType entryType) {
        switch (entryType) {
            case ENTRY_EXPENDITURE:
                return "cost";
            default:
                return "income";
        }
    }
}
