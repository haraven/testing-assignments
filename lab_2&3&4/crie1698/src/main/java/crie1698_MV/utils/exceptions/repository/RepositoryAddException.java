package crie1698_MV.utils.exceptions.repository;

public class RepositoryAddException extends RuntimeException {
    public RepositoryAddException() {
    }

    public RepositoryAddException(String message) {
        super(message);
    }

    public RepositoryAddException(String message, Throwable cause) {
        super(message, cause);
    }

    public RepositoryAddException(Throwable cause) {
        super(cause);
    }
}
