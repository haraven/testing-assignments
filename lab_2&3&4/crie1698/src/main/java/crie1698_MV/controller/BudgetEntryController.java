package crie1698_MV.controller;


import crie1698_MV.model.BudgetEntry;
import crie1698_MV.repository.BudgetEntryRepository;

import java.util.List;
//import java.util.function.Predicate;

public class BudgetEntryController {
    private BudgetEntryRepository entryRepository;

    public BudgetEntryController(BudgetEntryRepository entryRepository) {
        this.entryRepository = entryRepository;
    }

    public void addEntry(BudgetEntry entry) {
        entryRepository.add(entry);
    }

    public List<BudgetEntry> getAllEntries() {
        return entryRepository.all();
    }

//    public List<BudgetEntry> filterEntries(Predicate<BudgetEntry> pred) {
//        return entryRepository.filter(pred);
//    }


    public BudgetEntryRepository getEntryRepository() {
        return entryRepository;
    }

    public List<BudgetEntry> budgetEntriesForMember(Integer memberId) {
        return entryRepository.budgetEntriesForMember(memberId);
    }

    public void save(final String filename) {
        entryRepository.saveToFile(filename);
    }

    public String getSaveFile() {
        return entryRepository.getFilename();
    }
}
