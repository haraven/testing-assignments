package crie1698_MV.controller;

import crie1698_MV.model.CommunityMember;
import crie1698_MV.repository.CommunityMemberRepository;
import crie1698_MV.repository.FileRepository;
import crie1698_MV.utils.exceptions.validators.ValidationException;

import java.util.List;
//import java.util.function.Predicate;

public class CommunityMemberController {
    private CommunityMemberRepository memberRepository;

    public CommunityMemberController(CommunityMemberRepository memberRepository) {
        this.memberRepository = memberRepository;
    }

    public void addMember(CommunityMember member) throws ValidationException {
        memberRepository.add(member);
    }

    public List<CommunityMember> getAllMembers() {
        return memberRepository.all();
    }

    public CommunityMember findById(Integer ID) {
        return memberRepository.findById(ID);
    }

//    public List<CommunityMember> filterMembers(Predicate<CommunityMember> pred) {
//        return memberRepository.filter(pred);
//    }

    public CommunityMemberRepository getMemberRepository() {
        return memberRepository;
    }

    public void save(String filename) {
        memberRepository.saveToFile(filename);
    }

    public String getSaveFile() {
        return memberRepository.getFilename();
    }
}