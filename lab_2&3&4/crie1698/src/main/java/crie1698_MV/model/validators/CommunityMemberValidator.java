package crie1698_MV.model.validators;


import crie1698_MV.model.CommunityMember;
import crie1698_MV.utils.exceptions.validators.ValidationException;

import java.util.ArrayList;
import java.util.List;

public class CommunityMemberValidator extends Validator<CommunityMember> {

    private Boolean containsDigits(String str) {
        for (int ch = 0; ch < str.length(); ++ch) {
            if (Character.isDigit(str.charAt(ch))) return true;
        }

        return false;
    }

    @Override
    public void validate(CommunityMember entity) throws ValidationException {
        List<String> errors = new ArrayList<>();
        if (entity.getId() < 0)
            errors.add("ID is negative");
        if (entity.getFirstName() == null || entity.getFirstName().equals("")) {
            errors.add("First name is null");
        } else {
            if (entity.getFirstName().length() > CommunityMember.NAME_LEN_MAX) {
                errors.add("First name length exceeds the " + CommunityMember.NAME_LEN_MAX.toString() + " character limit");
            }
            if (containsDigits(entity.getFirstName())) {
                errors.add("First name contains digits");
            }
        }
        if (entity.getLastName() == null || entity.getLastName().equals("")) {
            errors.add("Last name is null");
        } else {
            if (entity.getLastName().length() > CommunityMember.NAME_LEN_MAX) {
                errors.add("Last name length exceeds the " + CommunityMember.NAME_LEN_MAX.toString() + " character limit");
            }
            if (containsDigits(entity.getLastName())) {
                errors.add("Last name contains digits");
            }
        }

        if (errors.size() > 0) {
            String error = errorsToString(errors);
            throw new ValidationException(error);
        }
    }
}
