package crie1698_MV.model.validators;

import crie1698_MV.utils.exceptions.validators.ValidationException;

import java.util.List;

public abstract class Validator<T> {
    String errorsToString(List<String> errors) {
        StringBuilder sb = new StringBuilder();
        for (String error : errors) {
            sb.append(error)
                    .append("; ");
        }
        String res = sb.toString();
        if (res.length() > 0) {
            res = res.substring(0, res.length() - 2);
        }

        return res;
    }

    public abstract void validate(T entity) throws ValidationException;
}
