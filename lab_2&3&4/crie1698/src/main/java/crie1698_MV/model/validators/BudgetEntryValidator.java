package crie1698_MV.model.validators;

import crie1698_MV.model.BudgetEntry;
import crie1698_MV.model.CommunityMember;
import crie1698_MV.repository.CommunityMemberRepository;
import crie1698_MV.repository.Repository;
import crie1698_MV.utils.exceptions.validators.ValidationException;

import java.util.ArrayList;
import java.util.List;

public class BudgetEntryValidator extends Validator<BudgetEntry> {
    private CommunityMemberRepository memberRepository;

    public BudgetEntryValidator(CommunityMemberRepository memberRepository) {
        this.memberRepository = memberRepository;
    }

    @Override
    public void validate(BudgetEntry entity) throws ValidationException {
        List<String> errors = new ArrayList<>();
        if (entity.getMemberId() < 0 ||
                memberRepository.findById(entity.getMemberId()) == null) {
            errors.add("No community member having ID: " + entity.getMemberId().toString() + " exists");
        }
        if (entity.getValue() < 0) {
            errors.add("Budget entry value cannot be negative");
        }

        if (errors.size() > 0) {
            throw new ValidationException(errorsToString(errors));
        }
    }
}
