package crie1698_MV.model;

import crie1698_MV.utils.enums.BudgetEntryType;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class BudgetEntry {
    public static final Integer FIELD_COUNT = 4;
    public static final String DATE_FORMAT = "dd-mm-yyyy";

    private BudgetEntryType entryType;
    private Integer value;
    private Integer memberId;
    private Date dateRecorded;


    public BudgetEntry(BudgetEntryType entryType, Integer value, Integer memberId) {
        this.entryType = entryType;
        this.value = value;
        this.memberId = memberId;
        this.dateRecorded = new Date();
    }

    public BudgetEntry(BudgetEntryType entryType, Integer value, Integer memberId, Date date) {
        this.entryType = entryType;
        this.value = value;
        this.memberId = memberId;
        this.dateRecorded = date;
    }

    public void setType(BudgetEntryType newType) {
        entryType = newType;
    }

    public BudgetEntryType getType() {
        return entryType;
    }

    public void setValue(Integer newValue) {
        this.value = newValue;
    }

    public Integer getValue() {
        return value;
    }

    public void setMemberId(Integer newMember) {
        this.memberId = newMember;
    }

    public BudgetEntryType getEntryType() {
        return entryType;
    }

    public void setEntryType(BudgetEntryType entryType) {
        this.entryType = entryType;
    }

    public Date getDateRecorded() {
        return dateRecorded;
    }

    public Integer getMemberId() {
        return memberId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BudgetEntry that = (BudgetEntry) o;

        return Objects.equals(value, that.value) &&
                Objects.equals(memberId, that.memberId) &&
                entryType == that.entryType;
    }

    @Override
    public int hashCode() {
        Integer result = value;
        result = 31 * result + entryType.hashCode();
        result = 31 * result + memberId;
        return result;
    }

    @Override
    public String toString() {
        DateFormat df = new SimpleDateFormat(BudgetEntry.DATE_FORMAT);
        return '{' +
                "value = " + value +
                ", entry type = " + BudgetEntryType.toString(entryType) +
                ", member ID = " + memberId +
                ", date recorded = " + df.format(dateRecorded) +
                '}';
    }
}
